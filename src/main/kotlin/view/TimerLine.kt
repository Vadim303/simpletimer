package view

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import controller.Timers
import kotlinx.coroutines.delay
import kotlin.time.ExperimentalTime
import kotlin.time.TimeMark
import kotlin.time.TimeSource
import kotlin.time.milliseconds

@ExperimentalTime
@Composable
fun TimerLine(modifier: Modifier, timer: Timers.Timer) {
    var startTime by remember { mutableStateOf(null as TimeMark?) }
    var progress by remember { mutableStateOf(-1f) }
    var edit by remember { mutableStateOf(false) }
    var finish by remember { mutableStateOf(false) }

    LaunchedEffect(startTime) {
        startTime?.let { time ->
            while (time.elapsedNow() < timer.duration) {
                progress = (time.elapsedNow() / timer.duration).toFloat()
                (timer.duration / 1000).let {
                    delay (if (it < 50.milliseconds) 50.milliseconds else it)
                }
            }
            finish = true
            startTime = null
        }
        progress = -1f
    }
    Column(modifier.fillMaxWidth()) {
        Row {
            Text(
                "Timer '${timer.name}' (duration ${timer.duration})",
                Modifier.align(Alignment.CenterVertically).padding(8.dp),
                style = MaterialTheme.typography.h6
            )
            Spacer(Modifier.weight(1f))
            IconButton(Icons.Default.Done, "Run") { startTime = TimeSource.Monotonic.markNow() }
            IconButton(Icons.Default.Clear, "Stop") { startTime = null }
            IconButton(Icons.Default.Edit, "Edit") { edit = true }
            IconButton(Icons.Default.Delete, "Delete") { Timers.delete(timer) }
        }
        if (progress < 0f)
            Divider(Modifier.fillMaxWidth().padding(2.dp))
        else
            LinearProgressIndicator(progress, Modifier.padding(4.dp).fillMaxWidth().preferredHeight(8.dp))
        if (edit) {
            EditTimer(timer) { edit = false }
        }
        if (finish) {
            TimerFinished(timer) { finish = false }
        }
    }
}
