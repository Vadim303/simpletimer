package view

import androidx.compose.desktop.DesktopMaterialTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DesktopDialogProperties
import androidx.compose.ui.window.Dialog
import bufferedImage
import controller.Resources
import controller.Timers
import kotlin.time.ExperimentalTime
import kotlin.time.seconds

@ExperimentalTime
@Composable
fun EditTimer(editTimer: Timers.Timer? = null, onClose: () -> Unit) {
    var timer by remember {
        mutableStateOf(editTimer ?: Timers.Timer("Unnamed", 60.0))
    }
    var name by remember {
        mutableStateOf(timer.name)
    }
    var duration by remember {
        mutableStateOf(timer.duration)
    }

    Dialog(
        onDismissRequest = onClose,
        properties = DesktopDialogProperties(
            title = "Edit ${timer.name}",
            icon = Resources.icon.bufferedImage()
        )
    ) {
        DesktopMaterialTheme(colors = Resources.colors) {
            Scaffold(
                modifier = Modifier.fillMaxSize(),
                topBar = {
                    TopAppBar { Text("Edit timer", Modifier.padding(8.dp)) }
                },
                bottomBar = {
                    ButtonBar(onClose) { timer = Timers.edit(timer, name, duration) }
                }
            ) {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    TextField(
                        value = name,
                        onValueChange = { value ->
                            name = value
                        },
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text("Timer name:") },
                        placeholder = { Text("<enter timer name>") },
                        isErrorValue = derivedStateOf { name.isEmpty() }.value
                    )
                    Divider()
                    TextField(
                        value = duration.inSeconds.toString(),
                        onValueChange = { value ->
                            duration = (value.toDoubleOrNull() ?: 0.0).seconds
                        },
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text("Duration:") },
                        trailingIcon = { Text(duration.toString()) },
                        isErrorValue = derivedStateOf { duration.inSeconds <= 0.0 }.value
                    )
                }
            }
        }
    }
}

@Composable
fun ButtonBar(onClose: () -> Unit, onApply: () -> Unit) {
    BottomAppBar {
        Spacer(Modifier.weight(1f))
        IconButton(Icons.Default.Close, "Cancel", onClose)
        IconButton(Icons.Default.Done, "Apply") {
            onApply()
            onClose()
        }
    }
}
