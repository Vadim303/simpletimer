package view

import androidx.compose.desktop.AppManager
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomAppBar
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlin.time.ExperimentalTime

@ExperimentalTime
@Composable
fun BottomBar() = BottomAppBar {
    var addTimer by remember { mutableStateOf(false) }

    IconButton(Icons.Default.AddCircle, "Add timer") { addTimer = true }
    Spacer(Modifier.weight(1f))
    IconButton(Icons.Default.ExitToApp, "Close") { AppManager.exit() }
    if (addTimer) {
        EditTimer() { addTimer = false }
    }
}
