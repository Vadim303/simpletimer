package view

import androidx.compose.desktop.DesktopMaterialTheme
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DesktopDialogProperties
import androidx.compose.ui.window.Dialog
import controller.Resources
import controller.Timers
import bufferedImage
import kotlin.time.ExperimentalTime

@ExperimentalTime
@Composable
fun TimerFinished(timer: Timers.Timer, onClose: () -> Unit) {
    Dialog(
        onDismissRequest = onClose,
        properties = DesktopDialogProperties(
            title = "${timer.name} finish",
            icon = Resources.icon.bufferedImage(),
            undecorated = true
        )
    ) {
        DesktopMaterialTheme(colors = Resources.colors) {
            Scaffold(
                modifier = Modifier.border(1.dp, Resources.colors.primary),
                topBar = {
                    TopAppBar { Text("Timer finished", Modifier.padding(8.dp)) }
                },
                bottomBar = {
                    BottomAppBar {
                        Spacer(Modifier.weight(1f))
                        IconButton(Icons.Default.Close, "Close", onClose)
                    }
                }
            ) {
                Box(Modifier.fillMaxSize()) {
                    Text(
                        text = "${timer.name} finished after ${timer.duration}",
                        modifier = Modifier.align(Alignment.Center),
                        style = MaterialTheme.typography.h5
                    )
                }
            }
        }
    }
}