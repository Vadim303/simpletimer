package view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun TopBar() {
    TopAppBar {
        Column(Modifier.fillMaxWidth()) {
            Text(
                text = "Available timers:",
                modifier = Modifier.padding(4.dp),
                style = MaterialTheme.typography.h4
            )
        }
    }
}