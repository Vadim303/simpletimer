package view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import controller.Timers
import kotlin.time.ExperimentalTime

@ExperimentalTime
@Composable
fun Main() {
    val averageScrollSize = 64.dp

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = { TopBar() },
        bottomBar = { BottomBar() }
    ) {
        Box(modifier = Modifier.fillMaxSize()) {
            if (Timers().size == 0) {
                Text("No timers found")
            } else {
                LazyColumn(
                    modifier = Modifier.fillMaxSize().padding(8.dp),
                ) {
                    val timers by derivedStateOf { Timers().toList() }
                    items(timers.size) { index ->
                        timers.getOrNull(index)?.let {
                            TimerLine(Modifier.height(averageScrollSize), it)
                        }
                    }
                }
            }
        }
    }

}