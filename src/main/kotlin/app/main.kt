import androidx.compose.desktop.DesktopMaterialTheme
import androidx.compose.desktop.Window
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.unit.IntSize
import controller.Resources
import controller.Timers
import org.jetbrains.skija.Image
import view.Main
import java.awt.image.BufferedImage
import kotlin.time.ExperimentalTime

@ExperimentalTime
fun main() {
    Resources.load()
    Timers.setPath(Resources.configPath)

    Window(
        title = "Simple Timer",
        size = IntSize(1000, 600),
        icon = Resources.icon.bufferedImage(),
        onDismissRequest = Timers::save,
        //undecorated = true
    ) {
        SideEffect { Timers.load() }

        DesktopMaterialTheme(
            colors = Resources.colors
        ) {
            Main()
        }
    }
}

fun Image.bufferedImage(): BufferedImage {
    val bufferedImage = BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_ARGB)
    val intArray = IntArray(this.width * this.height)
    this.asImageBitmap().readPixels(intArray)
    bufferedImage.setRGB(0, 0, this.width, this.height, intArray, 0, this.width)
    return bufferedImage
}