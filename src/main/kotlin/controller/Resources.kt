package controller

import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color
import org.jetbrains.skija.Image
import org.jetbrains.skija.Surface

object Resources {
    const val appName = "SimpleTimer"
    const val iconResource = "images/SimpleTimer.png"

    lateinit var configPath: String
    lateinit var icon: Image
    val colors = scheme0c31ThWs0g0g0()

    fun load() {
        configPath = "config.json"

        icon = try {
            val resource = Thread.currentThread()
                .contextClassLoader
                .getResource(iconResource)
            requireNotNull(resource) { "Resource $iconResource not found" }
            val bytes = resource.openStream().readBytes()
            Image.makeFromEncoded(bytes)
        } catch (err: Exception) {
            println("Unable to load application icon")
            val surface = Surface.makeRasterN32Premul(32, 32)
            surface!!.makeImageSnapshot()
        }
    }

    private fun scheme0c31ThWs0g0g0() = lightColors(
        // https://colorscheme.ru/#0c31ThWs0g0g0
        primary = Color(0xFF295975),
        primaryVariant = Color(0xFF43677C),
        secondary = Color(0xFF6FA637),
        secondaryVariant = Color(0xFF85B05A),
        error = Color(0xFFB85A3D),
        onError = Color(0xFFE79B83),
        // https://colorscheme.ru/#0c31T1BhWw0w0
        background = Color(0xFFCACFC4),
        surface = Color(0xFFE4E7E1)
    )
}