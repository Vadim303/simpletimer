package controller

import androidx.compose.runtime.mutableStateListOf
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.seconds

@ExperimentalTime
object Timers {
    private var path: Path = Paths.get(".")
    private val timers = mutableStateListOf<Timer>()

    operator fun invoke() = timers.also { println("getting ${it.size} timers") }

    fun edit(timer: Timer, name: String, duration: Duration): Timer {
        var index = timers.indexOfFirst { it.id == timer.id }
        println("Editing timer index $index")
        if (index >= 0) timers[index] = Timer(name, duration.inSeconds, timer.id)
        else {
            timers.add(Timer(name, duration.inSeconds, timer.id))
            index = timers.lastIndex
            println("Added timer at index $index")
        }
        return timers[index]
    }

    fun delete(timer: Timer) {
        timers.removeIf { it.id == timer.id }
    }

    fun setPath(configPath: String) {
        path = Paths.get(configPath)
    }

    fun save() {
        println("Saving timers")
        File(path.toUri()).writeText(
            Json.encodeToString(timers.toList())
        )
    }

    fun load() {
        println("Loading timers")
        timers.clear()
        val file = File(path.toUri())
        if (file.exists()) {
            timers.addAll(
                Json.decodeFromString<List<Timer>>(
                    file.readText()
                )
            )
        }
    }

    @ExperimentalTime
    @Serializable
    data class Timer(var name: String, var seconds: Double, var id: Long = Random.nextLong()) {
        @Transient
        val duration = seconds.seconds
    }
}